#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

#define STUDENTS_COUNT 10

class Student {
	public:
		string studentNo;		
		void setStudentNo(string studentNo) {
			this->studentNo = studentNo;
		}
		
		string getStudentNo() {
			return this->studentNo;
		}
		
	public:
		string studentName;
		void setStudentName(string studentName){
			this->studentName = studentName;
			
		}
		string getStudentName(){
			return this->studentName;
		}
		public:
		string studentSName;
		void setStudentSName(string studentSName){
			this->studentSName = studentSName;
			
		}
		string getStudentSName(){
			return this->studentSName;
		}	
		
		public:
		string studentStatus;
		void setStudentStatus(string studentStatus){
			this->studentStatus = studentStatus;
			
		}
	
		string getStudentStatus(){
			return this->studentStatus;
		}


};

string getRandomStudentNumber() {
	ostringstream ss;
	int randomNumber = rand() % 2000 + 37000;
	
	ss << randomNumber;
	
	return ss.str();
}
string getRandomStudentName(){

	
 
 string randomName[]={"Micha³","Maciek","Tomek","Adam","Eugeniusz","Robert"};
 
 int name= rand ()% 6;

return randomName[name];


}

string getRandomStudentSName(){

 string randomSName[]={" Pawlak"," Ziemny"," Paleta"," Gorek"," Stefanski"," Bofoski"};
 
 int sname = rand() % 6;
 

return randomSName[sname];
}

string getRandomStudentStatus(){
	
	ostringstream ss;
	int randomNumber =rand() % 2;
	ss << randomNumber;
	return  ss.str();
}


int main() {
	vector<Student> students;
	
	for(int i = 0; i < STUDENTS_COUNT; i++) {
		Student student;
		student.setStudentNo(getRandomStudentNumber());
		student.setStudentName(getRandomStudentName());
		student.setStudentSName(getRandomStudentSName());
		student.setStudentStatus(getRandomStudentStatus());
		students.push_back(student);
	}
	
	cout  << "Students group have been filled." << endl << endl;
	
	for(int i = 0; i < students.size(); i++) {
		Student student = students.at(i);
		cout <<"indeks: "<< student.getStudentNo() << endl;
		cout << "imie i nazwisko: "<< student.getStudentName() << student.getStudentSName() << endl;
		
		cout << "status: "<< student.getStudentStatus() << endl;
 	}
	
	return 0;
}